using System;

namespace EpamHomeTasks
{
    class Rectangle
    {
        //Private feids _x1, _y1 - coordinates of left upper corner, _x2, _y2 - coordinates of right bottom corner
        private int _x1;
        private int _x2;
        private int _y1;
        private int _y2;
        
        //Constructor
        public Rectangle(int x1, int y1, int x2, int y2)
        {
            this._x1 = x1;
            this._x2 = x2;
            this._y1 = y1;
            this._y2 = y2;
        }
        
        //Public property for each field
        public int Y2
        {
            get { return _y2; }
        }

        public int Y1
        {
            get { return _y1; }
        }

        public int X2
        {
            get { return _x2; }
        }

        public int X1
        {
            get { return _x1; }
        }

        //PUBLIC METHODS
        //Move rectangle on new position, newX, newY - new coordinates of left upper corner
        public void Move(int newX, int newY)
        {
            this._x2 += newX - this._x1;
            this._y2 += newY - this._y1;
            this._x1 = newX;
            this._y1 = newY;
        }

        //Changing length and height of current rectangle
        public void ChangeSize(int length, int height)
        {
            if(length >= 0 && height >= 0)
            {
                this._x2 = this._x1 + length;
                this._y2 = this._y1 - height;
            }
            else
            {
                throw new Exception("Length and height must be > 0");
            }
        }

        //Return smallest rectangle which includes two given rectangles
        public static Rectangle SmallestRect(Rectangle r1, Rectangle r2)
        {
            int newX1, newY1, newX2, newY2;

            newX1 = Math.Min(r1.X1, r2.X1);
            newY1 = Math.Max(r1.Y1, r2.Y1);
            newX2 = Math.Max(r1.X2, r2.X2);
            newY2 = Math.Min(r1.Y2, r2.Y2);

            return new Rectangle(newX1, newY1, newX2, newY2);
        }
        
        //Return rectangle as a intersection of two given rectangles
        public static Rectangle Intersection(Rectangle r1, Rectangle r2)
        {
            if ((r1.Y2 > r2.Y1) || (r2.Y2 > r1.Y1) || (r1.X2 < r2.X1) || (r2.X2 < r1.X1))
            {
                throw new Exception("Rectangles does not crossing");
            }
            else
            {
                int newX1 = Math.Max(r1.X1, r2.X1);
                int newY1 = Math.Min(r1.Y1, r2.Y1);
                int newX2 = Math.Min(r1.X2, r2.X2);
                int newY2 = Math.Max(r1.Y2, r2.Y2);

                return new Rectangle(newX1, newY1, newX2, newY2);
            }
        }

        //Shows main info about rectangle
        public override string ToString()
        {
            return string.Format("X1 = {0}, Y1 = {1}, X2 = {2}, Y2 = {3}\n", this._x1, this._y1, this._x2, this._y2);
        }

    }
    
    
    
    class Program
    {
        static void Main()
        {
            Rectangle first_rect = new Rectangle(0, 4, 3, 0);
            Rectangle second_rect = new Rectangle(-4, 3, 2, 1);
             
            Console.WriteLine("First Rectangle: {0}", first_rect);
            Console.WriteLine("Second Rectangle: {0}", second_rect);

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Smallest rectangle which include first and second rectangles: {0}", Rectangle.SmallestRect(first_rect, second_rect));
            
            try
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Intersection of two rectangles: {0}", Rectangle.Intersection(second_rect, first_rect));
                       
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Move first rectangle on 3, 3:");
                first_rect.Move(3, 3);
                Console.WriteLine(first_rect);
                
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("Change size of second rectangle:");
                second_rect.ChangeSize(4, 4);
                Console.WriteLine(second_rect);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }
    }
}

